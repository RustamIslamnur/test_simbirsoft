package com.example.test_simbirsoft.Data

import com.google.gson.annotations.SerializedName
import java.sql.Timestamp

data class Info(@SerializedName("id")val id: Int,
                @SerializedName ("datestart")val datestart: Timestamp,
                @SerializedName ("datefinish")val datefinish: Timestamp,
                @SerializedName ("name") val name: String,
                @SerializedName ("description")  val description: String)