package com.example.test_simbirsoft.Presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.CalendarView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test_simbirsoft.Domain.MainRepository
import com.example.test_simbirsoft.Presentation.adapter.Adapter
import com.example.test_simbirsoft.R
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val calendarView = findViewById<CalendarView>(R.id.calendarView)
        calendarView?.setOnDateChangeListener{view, year, month, dayOfMonth ->
            val msg = "Selected date is" + dayOfMonth + "/" + (month + 1) + "/" + year
            Toast.makeText(this@MainActivity, msg, Toast.LENGTH_LONG).show()
        }


        val recyclerView: RecyclerView = findViewById(R.id.recyclerview)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = Adapter(generateFakeValues())






        val bad = MainRepository.primitives()


        val gson = Gson()
        val input = gson.toJson(bad)
        Log.d("!!!!!!!","$input")




        val output = gson.fromJson(input, MainActivity.primitives::class.java)
        Log.d("!!!!!!!","$output")

    }

    internal class primitives(){
       @SerializedName ("id")val id = 1
        @SerializedName("name")val name = "kkkkkk"

    }
    private fun generateFakeValues(): List<String>{
        val values = mutableListOf<String>()
        for(i in 0..100){
            values.add("$i element")
        }
        return values
    }
}