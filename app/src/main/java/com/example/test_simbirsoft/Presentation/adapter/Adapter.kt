package com.example.test_simbirsoft.Presentation.adapter


import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.test_simbirsoft.R

class Adapter(private val values: List<String>): RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun getItemCount() = values.size

    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(p0?.context).inflate(R.layout.item_view,p0,false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(p0: ViewHolder, position: Int) {
        p0?.textView?.text = values[position]
    }



    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!){
        var textView: TextView?=null
        init {
            textView = itemView?.findViewById(R.id.textView)
        }
    }

}